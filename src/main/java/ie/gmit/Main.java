package ie.gmit;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

import javax.swing.*;
import java.io.IOException;

@RestController
@EnableAutoConfiguration
public class Main {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new GUi();
            }
        });
        SpringApplication.run(Main.class, args);
    }
}