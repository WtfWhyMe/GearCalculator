package ie.gmit;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GUi {

    // Main GUI Jframe
    JFrame frame;

    // declare controls used
    private JButton addCommissionBtn;
    private JButton addDepositBtn;
    private JButton materialSubtractBtn;
    private JButton materialAdditionBtn;
    private JButton calculateBtn;
    private JLabel gearLabel;
    private JLabel materialsLabel;
    private JLabel commissionUnitsLabel;
    private JLabel depositUnitsLabel;
    private JTextField gearValueTxtField;
    private JTextField commissionTxtField;
    private JTextField depositTxtField;

    private final int defaultX = 20;
    private final int defaultY = 20;
    // private ints to set default window size
    private final int defaultWidth = 700;
    private final int defaultHeight = 500;

    //TextFields for calculation of total material cost
    private JTextField materialCost1;
    private JTextField materialCost2;
    private JTextField materialCost3;
    private JTextField materialCost4;
    private JTextField materialCost5;
    private JTextField materialCost6;
    private JTextField materialCost7;
    private JTextField materialCost8;

    private JTextField quantity1;
    private JTextField quantity2;
    private JTextField quantity3;
    private JTextField quantity4;
    private JTextField quantity5;
    private JTextField quantity6;
    private JTextField quantity7;
    private JTextField quantity8;

    private JPanel gearValuePanel;
    private JPanel materialsPanel;
    private JPanel commissionDepositPanel;
    private JPanel calculateBtnPanel;
    private JPanel materialCostPanel;
    private JPanel materialQuantityPanel;

    // count variables used for displaying items to GUI
    private int addSubtractCount = 0;
    private int commissionCount = 0;
    private int depositCount = 0;

    public final static Color lightGreen = new Color(160, 255, 160);
    public final static Color lightBlue = new Color(160, 160, 255);

    private static final String
            GEAR_PRICE = "Gear selling price",
            MATERIAL_COST_STRING = "Enter material Cost",
            QUANTITY_STRING = "Enter quantity";

    //set to true if environment is headless, ie. can be run without user input devices
    private static boolean isHeadless;

    private HashMap textFieldsHashMap;
    private HashMap labelHashMap;
    private HashMap buttonsHashMap;

    // Constructor
    public GUi(boolean isHeadless){
        this.isHeadless = isHeadless;
        runGUI();
    }

    public GUi(){
        this.isHeadless = false;
        runGUI();
    }

    public void runGUI(){
        if (!isHeadless) {
            JFrame.setDefaultLookAndFeelDecorated(true);
            frame = new JFrame("Gear Calculator");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setResizable(false);
            frame.setLayout(new GridLayout(2, 2, 0, 0));
        }
        materialCostPanel = new JPanel();
        materialQuantityPanel = new JPanel();
        gearValuePanel = new JPanel();
        materialsPanel = new JPanel();
        commissionDepositPanel = new JPanel();
        calculateBtnPanel = new JPanel();

        materialsPanel.setLayout(new GridLayout(1,1,0,0));
        gearValuePanel.setBackground(lightGreen);
        materialsPanel.setBackground(lightBlue);
        commissionDepositPanel.setBackground(lightBlue);
        calculateBtnPanel.setBackground(lightGreen);
        materialQuantityPanel.setBackground(lightBlue);
        materialCostPanel.setBackground(lightBlue);

        //Creating buttons for the application
        materialAdditionBtn = new JButton("Add");
        calculateBtn = new JButton("Calculate");
        materialSubtractBtn = new JButton("Subtraction");
        addDepositBtn = new JButton("       Add Deposit         ");
        addCommissionBtn = new JButton("     Add  Commission       ");

        materialCost1 = new JTextField(MATERIAL_COST_STRING,10);
        materialCost2 = new JTextField(MATERIAL_COST_STRING,10);
        materialCost3 = new JTextField(MATERIAL_COST_STRING,10);
        materialCost4 = new JTextField(MATERIAL_COST_STRING,10);
        materialCost5 = new JTextField(MATERIAL_COST_STRING,10);
        materialCost6 = new JTextField(MATERIAL_COST_STRING,10);
        materialCost7 = new JTextField(MATERIAL_COST_STRING,10);
        materialCost8 = new JTextField(MATERIAL_COST_STRING,10);


        //Creating Textfields for the application
        gearValueTxtField = new JTextField("Enter gear value",10);
        commissionTxtField = new JTextField("Please enter Commission",20);
        depositTxtField = new JTextField("Please enter deposit",20);

        quantity1 = new JTextField(QUANTITY_STRING,10);
        quantity2 = new JTextField(QUANTITY_STRING,10);
        quantity3 = new JTextField(QUANTITY_STRING,10);
        quantity4 = new JTextField(QUANTITY_STRING,10);
        quantity5 = new JTextField(QUANTITY_STRING,10);
        quantity6 = new JTextField(QUANTITY_STRING,10);
        quantity7 = new JTextField(QUANTITY_STRING,10);
        quantity8 = new JTextField(QUANTITY_STRING,10);

        //Creating Labels for  the application
        gearLabel = new JLabel();
        gearLabel.setText(GEAR_PRICE);
        gearLabel.setHorizontalTextPosition(JLabel.LEFT);
        gearLabel.setVerticalTextPosition(JLabel.TOP);

        materialsLabel = new JLabel();
        materialsLabel.setText("   Materials");

        commissionUnitsLabel = new JLabel();
        commissionUnitsLabel.setText("%   ");
        commissionUnitsLabel.setHorizontalTextPosition(JLabel.LEFT);
        commissionUnitsLabel.setVerticalTextPosition(JLabel.CENTER);

        depositUnitsLabel = new JLabel();
        depositUnitsLabel.setText("gold");
        depositUnitsLabel.setHorizontalTextPosition(JLabel.RIGHT);
        depositUnitsLabel.setVerticalTextPosition(JLabel.BOTTOM);

        //adding labels to the application
        gearValuePanel.add(gearLabel);
        gearValuePanel.add(gearValueTxtField);

        materialQuantityPanel.add(materialsLabel);
        materialCostPanel.add(materialAdditionBtn);
        materialCostPanel.add(materialSubtractBtn);

        commissionDepositPanel.add(addCommissionBtn);
        commissionDepositPanel.add(addDepositBtn);
        calculateBtnPanel.add(calculateBtn);

        // Action Listener for Commission button
        addCommissionBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                commissionCount++;
                if (commissionCount %2 == 0) {
                    commissionDepositPanel.remove(commissionUnitsLabel);
                    commissionDepositPanel.remove(commissionTxtField);
                }
                else {
                    commissionDepositPanel.add(commissionTxtField);
                    commissionDepositPanel.add(commissionUnitsLabel);
                }

                if (!isHeadless) {
                    frame.repaint();
                    frame.validate();
                }
            }
        }); // end of commission action listener

        // Action Listener for Deposit button
        addDepositBtn.addActionListener(new ActionListener() {
             @Override
             public void actionPerformed(ActionEvent e) {

                 depositCount++;
                 if (depositCount %2 == 0) {
                     commissionDepositPanel.remove(depositUnitsLabel);
                     commissionDepositPanel.remove(depositTxtField);
                 }
                 else {
                     commissionDepositPanel.add(depositTxtField);
                     commissionDepositPanel.add(depositUnitsLabel);
                 }

                 if (!isHeadless) {
                     frame.repaint();
                     frame.validate();
                 }
             }
         }
        ); // end of deposit action listener

        // action listener for add button
        materialAdditionBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addSubtractCount++;
                if(addSubtractCount >=9) {
                    addSubtractCount = 8;
                }
                if(addSubtractCount == 1){
                    materialQuantityPanel.add(materialCost1);
                    materialCostPanel.add(quantity1);
                }
                else if(addSubtractCount == 2) {
                    materialQuantityPanel.add(materialCost2);
                    materialCostPanel.add(quantity2);
                }
                else if(addSubtractCount == 3) {
                    materialQuantityPanel.add(materialCost3);
                    materialCostPanel.add(quantity3);
                }
                else if(addSubtractCount == 4) {
                    materialQuantityPanel.add(materialCost4);
                    materialCostPanel.add(quantity4);
                }
                else if(addSubtractCount == 5) {
                    materialQuantityPanel.add(materialCost5);
                    materialCostPanel.add(quantity5);
                }
                else if(addSubtractCount == 6){
                    materialQuantityPanel.add(materialCost6);
                    materialCostPanel.add(quantity6);
                }
                else if(addSubtractCount == 7){
                    materialQuantityPanel.add(materialCost7);
                    materialCostPanel.add(quantity7);
                }
                else if(addSubtractCount == 8){
                    materialQuantityPanel.add(materialCost8);
                    materialCostPanel.add(quantity8);
                }

                if (!isHeadless) {
                    frame.repaint();
                    frame.validate();
                }
            }
        }); // end of add button listener

        // Action listener for subtract button
        materialSubtractBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(addSubtractCount <=0) {
                    addSubtractCount = 1;
                }
                if(addSubtractCount == 1) {
                    materialQuantityPanel.remove(materialCost1);
                    materialCostPanel.remove(quantity1);
                }
                else if(addSubtractCount == 2) {
                    materialQuantityPanel.remove(materialCost2);
                    materialCostPanel.remove(quantity2);
                }
                else if(addSubtractCount == 3) {
                    materialQuantityPanel.remove(materialCost3);
                    materialCostPanel.remove(quantity3);
                }
                else if(addSubtractCount == 4) {
                    materialQuantityPanel.remove(materialCost4);
                    materialCostPanel.remove(quantity4);
                }
                else if(addSubtractCount == 5) {
                    materialQuantityPanel.remove(materialCost5);
                    materialCostPanel.remove(quantity5);
                }
                else if(addSubtractCount == 6) {
                    materialQuantityPanel.remove(materialCost6);
                    materialCostPanel.remove(quantity6);
                }
                else if(addSubtractCount == 7) {
                    materialQuantityPanel.remove(materialCost7);
                    materialCostPanel.remove(quantity7);
                }
                else if(addSubtractCount == 8) {
                    materialQuantityPanel.remove(materialCost8);
                    materialCostPanel.remove(quantity8);
                }
                addSubtractCount--;

                if (!isHeadless) {
                    frame.repaint();
                    frame.validate();
                }
            }
        }); // end of action listener for subtract button

        // action listener for Calculate button
        calculateBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GearCalculator gc = extractData();
                gc.calculateTotalMaterialPrice();
                gc.calculateTotalMoneyReceived();
                gc.calculateProfitOrLoss();

                if (!isHeadless) {
                    new ResultFrame(gc.getSellPrice(), gc.getTotalMaterialCost(), gc.getCommission(), gc.getDeposit(), gc.getProfitOrLoss());
                    frame.dispose();
                }
            }
        }); // end of calculate action listener

        if (!isHeadless) {
            frame.add(gearValuePanel);
            materialsPanel.add(materialQuantityPanel);
            materialsPanel.add(materialCostPanel);
            frame.add(materialsPanel);
            frame.add(commissionDepositPanel);
            frame.add(calculateBtnPanel);
            frame.setBounds(defaultX, defaultY, defaultWidth, defaultHeight);
            frame.setVisible(true);
        }

        //getting all text fields & labels to be mapped
        setTextFieldHashMap();
        setLabelHashMap();
        setButtonsHashMap();

    } // end of runGUI

    public double getDoubleValueFromTextField(String s){
        try {
            double doubleValue = Double.parseDouble(s);
            return doubleValue;
        } catch (NumberFormatException e) {
            System.out.println("Input String cannot be parsed to Double.");
        }
        return 0;
    }

    public int getIntValueFromTextField(String s){
        try {
            int intValue = Integer.parseInt(s);
            return intValue;
        } catch (NumberFormatException e) {
            System.out.println("Input String cannot be parsed to Double.");
        }
        return 0;
    }
    // extracting all data from textFields and put it into GearCalculator class
    public GearCalculator extractData(){
        GearCalculator gc = new GearCalculator();
        List<Double> materialList = new ArrayList<>();
        List<Integer> quantityList = new ArrayList<>();
        gc.setSellPrice(getDoubleValueFromTextField(gearValueTxtField.getText()));
        gc.setCommission(getDoubleValueFromTextField(commissionTxtField.getText()));
        gc.setDeposit(getDoubleValueFromTextField(depositTxtField.getText()));
        for(int i = 1; i < 9; i++){
            materialList.add(getDoubleValueFromTextField(this.getField("c"+ i)));
        }
        for(int i = 1; i < 9; i++){
            quantityList.add(getIntValueFromTextField(this.getField("q"+ i)));
        }
        gc.setMaterialList(materialList);
        gc.setQuantityList(quantityList);
        return gc;
    }

    public boolean returnIsHeadless() {
        return isHeadless;
    }

    private void setTextFieldHashMap(){
        textFieldsHashMap = new HashMap();
        textFieldsHashMap.put("c1", materialCost1);
        textFieldsHashMap.put("c2", materialCost2);
        textFieldsHashMap.put("c3", materialCost3);
        textFieldsHashMap.put("c4", materialCost4);
        textFieldsHashMap.put("c5", materialCost5);
        textFieldsHashMap.put("c6", materialCost6);
        textFieldsHashMap.put("c7", materialCost7);
        textFieldsHashMap.put("c8", materialCost8);
        textFieldsHashMap.put("q1", quantity1);
        textFieldsHashMap.put("q2", quantity2);
        textFieldsHashMap.put("q3", quantity3);
        textFieldsHashMap.put("q4", quantity4);
        textFieldsHashMap.put("q5", quantity5);
        textFieldsHashMap.put("q6", quantity6);
        textFieldsHashMap.put("q7", quantity7);
        textFieldsHashMap.put("q8", quantity8);
        textFieldsHashMap.put("gearValue",gearValueTxtField);
        textFieldsHashMap.put("commission", commissionTxtField);
        textFieldsHashMap.put("deposit", depositTxtField);
    }

    private void setLabelHashMap() {
        labelHashMap = new HashMap();
        labelHashMap.put("gearLabel", gearLabel);
        labelHashMap.put("materialsLabel", materialsLabel);
        labelHashMap.put("commissionLabel", commissionUnitsLabel);
        labelHashMap.put("depositLabel", depositUnitsLabel);
    }

    private void setButtonsHashMap(){
        buttonsHashMap = new HashMap();
        buttonsHashMap.put("add", materialAdditionBtn);
        buttonsHashMap.put("sub", materialSubtractBtn);
        buttonsHashMap.put("com", addCommissionBtn);
        buttonsHashMap.put("dep", addDepositBtn);
        buttonsHashMap.put("calc", calculateBtn);
    }

    // set text in JTextField by specifying field's
    // name and value
    public void setField(String fieldName, String value) {
        JTextField field = ( JTextField ) textFieldsHashMap.get(fieldName);
        field.setText(value);
    }

    // get text in JTextField by specifying field's name
    public String getField( String fieldName ) {
        JTextField field = (JTextField) textFieldsHashMap.get(fieldName);
        return field.getText();
    }

    // set text in JLabel by specifying field's
    // name and value
    public void setLabel(String labelName, String value) {
        JLabel label = ( JLabel ) labelHashMap.get(labelName);
        label.setText(value);
    }

    // get text in JLabel by specifying field's name
    public String getLabel( String labelName ) {
        JLabel label = (JLabel) labelHashMap.get(labelName);
        return label.getText();
    }

    public JButton getButton(String buttonName){
        JButton button = (JButton) buttonsHashMap.get(buttonName);
        return button;
    }
    public void setAddSubtractCount(int num){
        addSubtractCount = num;
    }
}