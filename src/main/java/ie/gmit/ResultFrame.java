//Software Engineering with Test
// Team Project - GearCalculator
// Rokas Cesiunas

// ResultFrame.java
// A JFrame class to display the result calculated from main GUI Frame
// Shows summary of data entered and calculated profit / loss

package ie.gmit;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ResultFrame {

    // private ints to set default location on screen when opened
    private final int defaultX = 20;
    private final int defaultY = 20;
    // private ints to set default window size
    private final int defaultWidth = 600;
    private final int defaultHeight = 350;

    // Result GUI Jframe
    JFrame frame;

    // panels to organize GUI
    private JPanel leftPanel, rightPanel, topPanel, bottomPanel;

    // return button
    private JButton retBtn;

    // HashMap to store JTextField references for quick access
    private HashMap fields;

    // static Strings that represent name of each text field.
    // These are placed on JLabels and used as keys in
    // HashMap fields.
    private static final String
            GEAR_SELL_PRICE = "Gear Price",
            TOTAL_MONEY_COST = "Total Money Cost (-)",
            GEAR_SELL_COMMISSION = "Gear Sell Commission (-)",
            GEAR_SELL_DEPOSIT = "Gear Sell Deposit (-)",
            PROFIT_LOSS = "Profit / Loss";

    // default row number for ResultJPanel
    private static final int frameRows = 4;

    // private ints to store data from main frame
    private double gearSalePrice;
    private double totalMaterialSaleCost;
    private double saleCommission;
    private double saleDeposit;
    private double totalProfit;

    //set to true if environment is headless, ie. can be run without user input devices
    private static boolean isHeadless;

    // Image taken from https://www.uhdpaper.com/2019/08/jaina-proudmoore-wow-fantasy-girl-4k_30.html
    Image img = Toolkit.getDefaultToolkit().getImage("JainaProudmoore.jpg");

    // construct default Result GUI
    public ResultFrame(double gearSalePrice, double totalMaterialSaleCost, double saleCommission, double saleDeposit, double totalProfit) {

        isHeadless = false;
        createResultFrame(gearSalePrice, totalMaterialSaleCost, saleCommission, saleDeposit, totalProfit);
    }

    // construct default Result GUI for use on testing
    public ResultFrame(boolean isHeadless, double gearSalePrice, double totalMaterialSaleCost, double saleCommission, double saleDeposit, double totalProfit) {

        this.isHeadless = isHeadless;
        createResultFrame(gearSalePrice, totalMaterialSaleCost, saleCommission, saleDeposit, totalProfit);
    }

    private void createResultFrame(double gearSalePrice, double totalMaterialSaleCost, double saleCommission, double saleDeposit, double totalProfit) {

        logger.log(Level.FINE, "Creating Result Frame GUI");

        this.gearSalePrice = gearSalePrice;
        this.totalMaterialSaleCost = totalMaterialSaleCost;
        this.saleCommission = saleCommission;
        this.saleDeposit = saleDeposit;
        this.totalProfit = totalProfit;

        if (!isHeadless) {
            JFrame.setDefaultLookAndFeelDecorated(true);
            frame = new JFrame("Calculated Result");
            frame.setLayout(new GridLayout(2, 1, 0, 0));

            frame.setContentPane(new JPanel() {
                @Override
                public void paintComponent(Graphics g) {
                    super.paintComponent(g);
                    g.drawImage(img, 0, 0, null);
                }
            });

            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }
        else {
            logger.log(Level.FINE, "Skipping creating GUI");
        }

        fields = new HashMap();

        // create main gui layout
        leftPanel = new JPanel();
        leftPanel.setLayout( new GridLayout( frameRows, 1));
        rightPanel = new JPanel();
        rightPanel.setLayout( new GridLayout( frameRows, 1));

        topPanel = new JPanel();
        topPanel.setLayout(new GridLayout(1, 2));
        topPanel.setBorder(BorderFactory.createEmptyBorder(5,0,5,0));
        topPanel.add(leftPanel);
        topPanel.add(rightPanel);

        topPanel.setOpaque(false);
        leftPanel.setOpaque(false);
        rightPanel.setOpaque(false);

        bottomPanel = new JPanel();
        bottomPanel.setLayout( new BorderLayout(5, 0));

        createRow(GEAR_SELL_PRICE);
        createRow(TOTAL_MONEY_COST);
        createRow(GEAR_SELL_COMMISSION);
        createRow(GEAR_SELL_DEPOSIT);
        createRow(PROFIT_LOSS);

        setField(GEAR_SELL_PRICE, String.format("%.2f", gearSalePrice));
        setField(TOTAL_MONEY_COST, String.format("%.2f", totalMaterialSaleCost));
        setField(GEAR_SELL_COMMISSION, String.format("%.2f", saleCommission));
        setField(GEAR_SELL_DEPOSIT, String.format("%.2f", saleDeposit));
        setField(PROFIT_LOSS, String.format("%.2f", totalProfit) + " Gold");

        // Based on value in PROFIT_LOSS text field, set colour
        JTextField field = (JTextField) fields.get(PROFIT_LOSS);
        if (totalProfit <= 0) {
            field.setBackground(Color.RED);
        }
        else
            field.setBackground(Color.GREEN);

        bottomPanel.setBackground(Color.orange);
        retBtn = new JButton("Return to Main Window");

        retBtn.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        System.out.println("returned");
                        new GUi(isHeadless);
                        if (!isHeadless)
                            frame.dispose();
                    }
                }
        );

        if (!isHeadless) {
            rightPanel.setBorder(BorderFactory.createMatteBorder(0, 3, 0, 0, Color.DARK_GRAY));
            bottomPanel.add(retBtn, BorderLayout.SOUTH);

            frame.add(topPanel);
            frame.add(bottomPanel);
            frame.setBounds(defaultX, defaultY, defaultWidth, defaultHeight);
            frame.setResizable(false);
            frame.setVisible(true);
        }
        else
            logger.log(Level.FINE, "Skipped creating GUI");

        logger.log(Level.FINE, "Result Frame created");
    }

    // set text in JTextField by specifying field's
    // name and value
    private void setField( String fieldName, String value ) {

        JTextField field =
                ( JTextField ) fields.get(fieldName);

        field.setText(value);
    }

    // get text in JTextField by specifying field's name
    public String getFieldText( String fieldName ) {

        JTextField field = (JTextField) fields.get(fieldName);
        return field.getText();
    }

    public JTextField getField ( String fieldName ) {

        JTextField field = (JTextField) fields.get(fieldName);
        return field;
    }

    // utility method used by constructor to create one row in
    // GUI containing JLabel and JTextField
    private void createRow( String name ) {

        logger.log(Level.FINE, "Creating Row");

        JLabel label = new JLabel( name, SwingConstants.RIGHT );
        if (!isHeadless)
            label.setFont(new Font("Lucinda Bright", Font.BOLD, 20));

        label.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.LIGHT_GRAY));
        if (name.equals(PROFIT_LOSS)) {
            label.setHorizontalAlignment(SwingConstants.CENTER);
            bottomPanel.add(label, BorderLayout.NORTH);
        }
        else {
            label.setHorizontalAlignment(SwingConstants.RIGHT);
            label.setOpaque(false);
            label.setForeground(Color.YELLOW);
            leftPanel.add(label);
        }

        JTextField field = new JTextField( 20 );
        field.setEditable(false);
        field.setHorizontalAlignment(SwingConstants.LEFT);

        if (!isHeadless)
            field.setFont(new Font("Lucida Bright", Font.BOLD, 30));

        field.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.LIGHT_GRAY));

        if (name.equals(PROFIT_LOSS)) {
            if (!isHeadless) {
                field.setFont(new Font("Monospaced", Font.BOLD, 55));
                field.setHorizontalAlignment(SwingConstants.CENTER);
            }
            bottomPanel.add(field, BorderLayout.CENTER);
        }
        else {
            field.setForeground(Color.WHITE);
            field.setOpaque(false);
            rightPanel.add(field);
        }

        fields.put(name, field);

        logger.log(Level.FINE, "Created Row");

    }

    public double getGearSalePrice() {
        return gearSalePrice;
    }

    public double getTotalMaterialSaleCost() {
        return totalMaterialSaleCost;
    }

    public double getSaleCommission() {
        return saleCommission;
    }

    public double getSaleDeposit() {
        return saleDeposit;
    }

    public double getTotalProfit() {
        return totalProfit;
    }

    public boolean returnIsHeadless() {
        return isHeadless;
    }

    public JButton getRetBtn() { return retBtn; }

    private static final Logger logger =
            Logger.getLogger(ResultFrame.class.getName());
}