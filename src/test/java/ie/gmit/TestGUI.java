package ie.gmit;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.awt.*;
import static org.junit.jupiter.api.Assertions.*;

public class TestGUI {

    private static final boolean runHeadlessMode = true;
    GUi g;

    @BeforeAll
    public static void setUpHeadlessMode() {
        if (runHeadlessMode) {
            // Set system property.
            // Call this BEFORE the toolkit has been initialized, that is,
            // before Toolkit.getDefaultToolkit() has been called.
            System.setProperty("java.awt.headless", "true");

            // This triggers creation of the toolkit.
            // Because java.awt.headless property is set to true, this
            // will be an instance of headless toolkit.
            Toolkit tk = Toolkit.getDefaultToolkit();
            // Standard beep is available.
            tk.beep();

            // Check whether the application is
            // running in headless mode.
            assert (GraphicsEnvironment.isHeadless());
        }
    }

    @BeforeEach
    void setupGUI() {
        g = new GUi(runHeadlessMode);
    }

    @Test
    public void testHeadlessSetup() {
        if (g.returnIsHeadless())
            assert(GraphicsEnvironment.isHeadless());
        else
            assert(!GraphicsEnvironment.isHeadless());
    }

    @Test
    public void testExceptionCatch(){
        Executable ex = new Executable() {
            @Override
            public void execute() throws Throwable {
                g.setField("gearValue","1000");
                g.extractData();
            }
        };
        assertDoesNotThrow(ex);
    }
    @Test
    public void testExceptionNoCatch(){
        Executable ex = new Executable() {
            @Override
            public void execute() throws Throwable {
              for(int i = 1; i < 9; i++){
                  g.setField("c"+ i,"1");
                  g.setField("q"+ i,"1");
              }
                g.setField("gearValue", "1");
                g.setField("commission", "1");
                g.setField("deposit", "1");
                g.extractData();
            }
        };
        assertDoesNotThrow(ex);
    }
    @Test
    public void testCorrectGearValueReturn(){
        g.setField("gearValue","1.5");
        double actualValue = g.getDoubleValueFromTextField(g.getField("gearValue"));
        assertEquals(1.5, actualValue);
    }
    @Test
    public void testCorrectDepositValueReturn(){
        g.setField("commission","1.5");
        double actualValue = g.getDoubleValueFromTextField(g.getField("commission"));
        assertEquals(1.5, actualValue);
    }
    @Test
    public void testCorrectCommissionValueReturn(){
        g.setField("deposit","1.5");
        double actualValue = g.getDoubleValueFromTextField(g.getField("deposit"));
        assertEquals(1.5, actualValue);
    }
    @Test
    public void testCorrectCostValueReturn(){
        double[] expected = new double[8];
        double[] actual = new double[8];
        for(int i = 1; i < 9; i++){
            g.setField("c"+ i,"1");
            expected[i-1] = 1;
        }
        for(int i = 1; i < 9; i++){
            actual[i-1] = (g.getDoubleValueFromTextField( g.getField("c"+ i)));
        }
        assertArrayEquals(expected, actual);
    }
    @Test
    public void testCorrectQuantityValueReturn(){
        int[] expected = new int[8];
        int[] actual = new int[8];
        for(int i = 1; i < 9; i++){
            g.setField("c"+ i,"1");
            expected[i-1] = 1;
        }
        for(int i = 1; i < 9; i++){
            actual[i-1] = (g.getIntValueFromTextField( g.getField("c"+ i)));
        }
        assertArrayEquals(expected, actual);
    }

    @Test
    public void testLabelsAreSetCorrect(){
        String[] actual = new String[4];
        String[] expected = new String[4];
        expected[0] = "Gear selling price";
        expected[1] = "   Materials";
        expected[2] = "%   ";
        expected[3] = "gold";
        actual[0] = g.getLabel("gearLabel");
        actual[1] = g.getLabel("materialsLabel");
        actual[2] = g.getLabel("commissionLabel");
        actual[3] = g.getLabel("depositLabel");

        assertArrayEquals(expected, actual);
    }



    @Test
    void testAddButtonActionListener() {
        for(int i = 0; i < 8; i++){
            g.getButton("add").doClick();
        }
      assertDoesNotThrow(() -> g.getButton("add").doClick());
    }
    @Test
    void testSubtractButtonActionListener() {
        g.setAddSubtractCount(8);
        for(int i = 0; i < 8; i++){
            g.getButton("sub").doClick();
        }
        assertDoesNotThrow(() -> g.getButton("sub").doClick());
    }
    @Test
    void testCommissionButtonActionListener() {
        assertDoesNotThrow(() -> g.getButton("com").doClick());
    }
    @Test
    void testDepositButtonActionListener() {
        assertDoesNotThrow(() -> g.getButton("dep").doClick());
    }
    @Test
    void testCalculateButtonThrowExceptionWhenGearValueNotSet(){
        Executable ex = new Executable() {
            @Override
            public void execute() throws Throwable {
                g.getButton("calc").doClick();
            }
        };
        assertThrows(IllegalArgumentException.class, ex);
    }

    @Test
    void testCalculateButtonThrowExceptionWhenMaterialsNotSet(){
        Executable ex = new Executable() {
            @Override
            public void execute() throws Throwable {
                g.setField("gearValue", "1000");
                g.getButton("calc").doClick();
            }
        };
        assertThrows(IllegalArgumentException.class, ex);
    }
    @Test
    void testCalculateButtonNotThrowException(){
        Executable ex = new Executable() {
            @Override
            public void execute() throws Throwable {
                g.setField("gearValue", "1000");
                g.setField("c1", "500");
                g.setField("q1", "5");
                g.getButton("calc").doClick();
            }
        };
        assertDoesNotThrow(ex);
    }
    @Test
    void testIfMaterialCostQuantIsZeroThrowException(){
        Executable ex = new Executable() {
            @Override
            public void execute() throws Throwable {
                g.setField("gearValue", "1000");
                g.setField("c1", "0");
                g.setField("q1", "0");
                g.getButton("calc").doClick();
            }
        };
        assertThrows(IllegalArgumentException.class, ex);
    }
}