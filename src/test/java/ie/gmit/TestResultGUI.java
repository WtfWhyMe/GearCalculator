package ie.gmit;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import java.awt.*;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestResultGUI {

    private static final boolean runHeadlessMode = true;
    ResultFrame r;

    private static final String
            GEAR_SELL_PRICE = "Gear Price",
            TOTAL_MONEY_COST = "Total Money Cost (-)",
            GEAR_SELL_COMMISSION = "Gear Sell Commission (-)",
            GEAR_SELL_DEPOSIT = "Gear Sell Deposit (-)",
            PROFIT_LOSS = "Profit / Loss";


    @BeforeAll
    public static void setUpHeadlessMode() {
        if (runHeadlessMode) {
            // Set system property.
            // Call this BEFORE the toolkit has been initialized, that is,
            // before Toolkit.getDefaultToolkit() has been called.
            System.setProperty("java.awt.headless", "true");

            // This triggers creation of the toolkit.
            // Because java.awt.headless property is set to true, this
            // will be an instance of headless toolkit.
            Toolkit tk = Toolkit.getDefaultToolkit();
            // Standard beep is available.
            tk.beep();

        }
    }

    @BeforeEach
    void setupGUI() {
        r = new ResultFrame(runHeadlessMode, 200,180,15,0,5);
    }

    @Test
    public void testHeadlessSetup() {

        // Check whether the application is
        // running in headless mode.
        if (r.returnIsHeadless())
            assert(GraphicsEnvironment.isHeadless());
        else
            assert(!GraphicsEnvironment.isHeadless());
    }

    @Test
    void testSettingDoubles() {
        double salePrice = r.getGearSalePrice();
        assertEquals(200, salePrice);

        double materialCost = r.getTotalMaterialSaleCost();
        assertEquals(180, materialCost);

        double commission = r.getSaleCommission();
        assertEquals(15, commission);

        double deposit = r.getSaleDeposit();
        assertEquals(0, deposit);

        double profit = r.getTotalProfit();
        assertEquals(5, profit);
    }

    @Test
    void testGuiTextDoubles() {
        String salePrice = r.getFieldText(GEAR_SELL_PRICE);
        assertEquals("200.00", salePrice);

        String materialCost = r.getFieldText(TOTAL_MONEY_COST);
        assertEquals("180.00", materialCost);

        String commission = r.getFieldText(GEAR_SELL_COMMISSION);
        assertEquals("15.00", commission);

        String deposit = r.getFieldText(GEAR_SELL_DEPOSIT);
        assertEquals("0.00", deposit);

        String profit = r.getFieldText(PROFIT_LOSS);
        assertEquals("5.00 Gold", profit);
    }

    @Test
    void testProfitLossBackgroundColour() {
        JTextField profitLossField = r.getField(PROFIT_LOSS);
        assertEquals(Color.GREEN, profitLossField.getBackground());

        ResultFrame r1 = new ResultFrame(true, 150.00, 250.10, 20.20, 30.30, -40.40);
        profitLossField = r1.getField(PROFIT_LOSS);
        assertEquals(Color.RED, profitLossField.getBackground());
    }

    @Test
    void testActionListener() {
        assertDoesNotThrow(() -> r.getRetBtn().doClick());
    }
}